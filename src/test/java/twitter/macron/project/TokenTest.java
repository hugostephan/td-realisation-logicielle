package twitter.macron.project;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import static org.junit.Assert.*;

@RunWith(Parameterized.class)
public class TokenTest {
    DateFormat format = new SimpleDateFormat("yyyyMMdd");
    Token token = new Token();

    String date;
    String parsingDate;

    @Parameterized.Parameters
    public static Collection<Object[]> params() {
        return Arrays.asList(
                new Object[] { "00021130","-0-0-0-2-1-1-3-0-" },
                new Object[] { "20180210","-2-0-1-8-0-2-1-0-" },
                new Object[] { "99991231","-9-9-9-9-1-2-3-1-"}
        );
    }

    public TokenTest(final String date, final String parsingDate) {
        this.date = date;
        this.parsingDate = parsingDate;
    }

    @Test
    public void generateTokenParameterizedTest() throws ParseException {
        String result = token.generateToken(format.parse(date));
        assertEquals(result,parsingDate);
    }


}