package twitter.macron.project.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import twitter.macron.project.configuration.TwitterConfig;
import twitter4j.Status;
import twitter4j.TwitterException;

import java.util.List;

@Controller
public class IndexController {
    @RequestMapping(value = {  "/",  "/index"})
    String index(Model model) throws TwitterException {
        TwitterConfig twitter = new TwitterConfig();
        List<Status> listTweet = twitter.setupTwitter();
        

        model.addAttribute("listtweet",listTweet);
        return "index";
    }
}
