package twitter.macron.project;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Token {
    private static final DateFormat FORMAT = new SimpleDateFormat("yyyyMMdd");

    public String generateToken(Date date){
        String formatted = FORMAT.format(date);
        return formatted.replaceAll("","-");
    }

}
